import java.util.Collections;
import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> storage;

   public static void main (String[] argum) {
      DoubleStack a = new DoubleStack();
      String s = null;
      //Double d = interpret(s);
     // a.pop();
      a.push(1);
      a.push(2);
      a.push(3);

       a.push(a.pop());
       double g = interpret("s 3 fg 1 +");
      System.out.println(a.toString());


   }

   DoubleStack() {
      storage = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack m = new DoubleStack();
      m.storage.addAll(storage);
      return m; // TODO!!! Your code here!
   }

   public boolean stEmpty() {
      if (storage.isEmpty()){
         return true;
      }
      return false; // TODO!!! Your code here!
   }

   public void push (double a) {
      storage.push(a);
   }

   public double pop() {
      if (stEmpty())
         throw new IndexOutOfBoundsException("Stack is empty.");
      return storage.pop(); // TODO!!! Your code here!
   }

   public void op (String s) {
      if (storage.size() < 2)
         throw new IndexOutOfBoundsException("Not enough elements.");
      if (!s.equals("+") && !s.equals("-") && !s.equals("*") && !s.equals("/") && !s.equals("SWAP"))
         throw new IllegalArgumentException("Illegal operation " + s);
      double op2 = pop();
      double op1 = pop();
      if (s.equals("+")) push(op2 + op1);
      if (s.equals("-")) push(op1 - op2);
      if (s.equals("*")) push(op2 * op1);
      if (s.equals("/")) push(op1 / op2);
       if (s.equals("SWAP")) {
           push(op2);
           push(op1);
       }
      // TODO!!!
   }
  
   public double tos() {
      if (stEmpty()){
         throw new IndexOutOfBoundsException("asd");
      }
      return storage.get(0); // TODO!!! Your code here!
   }

   @Override
   public boolean equals (Object o) {
      if(stEmpty() && ((DoubleStack)o).stEmpty()) return true;
      if(((DoubleStack)o).storage.size() != storage.size()) return false;
      for (int i = 0; i < storage.size(); i++) {
         Double el1 = ((DoubleStack)o).storage.get(i);
         Double el2 = storage.get(i);
         if (!el1.equals(el2)) return false;
      }
      return true; // TODO!!! Your code here!
   }

   @Override
   public String toString() {
      if (stEmpty()) return "";
      StringBuffer elements = new StringBuffer();
      for (int i = storage.size() - 1; i > -1 ; i--) {
         elements.append(storage.get(i) + " ");
      }
      return elements.toString(); // TODO!!! Your code here!
   }

   public static double interpret (String pol) {
      String available = "- + / * SWAP ROT";
      try {
         boolean f = pol.equals(null);
      } catch (NullPointerException e){
         throw new RuntimeException("Empty expression.");
      }
      if (pol.length() == 0) throw new RuntimeException("Empty expression.");
      pol = pol.trim();
      String[] elements = pol.split("\\s+");
      DoubleStack l = new DoubleStack();
      LinkedList<Double> numbers = new LinkedList<>();
      for (String s: elements) {
         try{
             l.push(Double.parseDouble(s));
            numbers.push(Double.parseDouble(s));
         } catch (NumberFormatException | NullPointerException e){
            if (!available.contains(s)) throw new RuntimeException("Illegal symbol " + s + " in expression " + pol);
            if (s.equals("ROT")){
               Double a = l.pop();
               Double b = l.pop();
               Double c = l.pop();
               l.push(b);
               l.push(a);
               l.push(c);
            }
            else {
               if (l.storage.size() < 2) throw new RuntimeException ("Not enough numbers for " + s + " in " + pol);
               l.op(s);
            }

         }
      }
      if (l.storage.size() > 1) throw new RuntimeException ("Too many numbers in expression " + pol);
      return l.pop(); // TODO!!! Your code here!
   }
}

